package com.sasaug.mmlsreader;

/*
*   Usually you don't really need an Application file but what important with this is that
*   you can initialise some static variable when the app starts up.
*
*   Sometimes, when Android reclaims the memory, the app will starts up form the page it left off
*   instead of the starting page. This is a bad behavior potrayed by Android but you just have to
*   deal with it. If you can code that setup stuff in your first activity, it won't be initialised
*   and might your app to crash due to NullPointerException.
*
*   The proper programming practice is write stuff in functions(Check User class) and check these
*   static variables if they are null, and if they are, then load it again
*
*   Application always gets started first no matter where you left the app off. Only trigger is the
*   app is closed tho, not resuming. You need to define this in AndroidManifest.xml in order for this
*   class to execute.
*
*   Try not to load heavy stuff like reading files etc in this class or else you will get an ugly
*   empty startup screen.
*
*   This class usually used to initialised like crash logging libraries or my personal preference,
*   an application wide context. Please understand context before using the Application context,
*   in some cases, you will need to use Activity's context instead of the Application context.
*   General case will be like those that exist in that Activity only like Toast etc should use
*   Activity context. Those reading files which is not Activity bound, can use Application context.
*
*
* */
import android.app.Application;
import android.content.Context;

public class MainApplication extends Application{
	static Context context;
	
	public static Context getContext(){
		return context;
	}
	
	public void onCreate(){
		context = this.getApplicationContext();
	}
}
