package com.sasaug.mmlsreader.util.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.sasaug.mmlsreader.util.MaterialManager;

/**
 * Created by sasaug on 10/27/14.
 */
public class RippleImageView extends ImageView implements View.OnClickListener{

    MaterialManager.TouchRippleHelper mRippleHelper;

    public RippleImageView(final Context context) {
        super(context);
        init();
    }

    public RippleImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RippleImageView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        mRippleHelper = new MaterialManager.TouchRippleHelper(this);
    }

    public MaterialManager.TouchRippleHelper getRippleHelper(){
        return mRippleHelper;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mRippleHelper.onDraw(canvas);
    }

    public void setRadius(float radius){
        mRippleHelper.setRadius(radius);
    }

    public void onClick(View view) {}

}
