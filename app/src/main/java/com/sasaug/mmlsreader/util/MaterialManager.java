package com.sasaug.mmlsreader.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import com.sasaug.mmlsreader.R;
import com.sasaug.mmlsreader.util.views.RippleTextView;

/**
 * Created by sasaug on 10/27/14.
 */
public class MaterialManager {


    /*
    *   TouchRippleHelper
    *
    *   To add touch ripple to a view
    *   1) You need to create a class which extends that view, override onTouchEvent, dispatchTouchEvent, onDraw
    *   2) Initialise TouchRippleHelper in view constructor
    *   3) Add setRadius into the view and point to setRadius in the helper
    *   4) NOTE: If you need to use setOnTouch on that view, please call onTouchEvent or else
    *           ripple wont work.
     *  5) Additionally you can set the ripple color using setColor and adjust alpha with setAlpha
    *
    *   Trick Explained:
    *   =================
    *   We listen to onTocuhEvent which will tell us when the user apply touch on it. Here we don't
    *   return true/false or else stuff won't work but instead call the view.onToucEvent so it will
    *   allow the parent view to process teh touch further. We also listen to UP/CANCEL event here.
    *   CANCEL trigger when u press and move your finger off
    *   the bounds.
    *
    *   Ripple is basically an translucent overlay which expands from 1 point and fill up the whole
    *   view then when you release, it actually fades off.
    *
    * */

    public static class TouchRippleHelper{

        private float mDownX;
        private float mDownY;
        private float mRadius;
        private Paint mPaint;
        private ObjectAnimator mAnimator;
        private ValueAnimator mFadeAnimator;
        private View v;

        private int alpha = 120;
        private long duration = 250;
        private boolean isRunning = false;
        private boolean isComplete = false;
        private boolean markForDelete = false;


        public TouchRippleHelper(View v){
            this.v = v;
            init();
        }

        public void init(){
            mPaint = new Paint();
            mPaint.setColor(Color.WHITE);
            setAlpha(alpha);

            try {
                ViewGroup group = (ViewGroup) v;
                group.setWillNotDraw(false);
            }catch(Exception e){}

            v.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    onTouchEvent(event);
                    return view.onTouchEvent(event);
                }
            });
        }

        public void onTouchEvent(MotionEvent event){
            if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                mDownX = event.getX();
                mDownY = event.getY();

                int maxRad = v.getWidth();
                if(v.getHeight() > maxRad)
                    maxRad = v.getHeight();

                setAlpha(alpha);

                mAnimator = ObjectAnimator.ofFloat(v, "radius", 0, maxRad*1.2f);
                mAnimator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {
                        markForDelete = false;
                        isRunning = true;
                        isComplete = false;
                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        isComplete = true;
                        if(markForDelete) {
                            fadeOff();
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {}

                    @Override
                    public void onAnimationRepeat(Animator animator) {}
                });
                mAnimator.setInterpolator(new AccelerateInterpolator());
                mAnimator.setDuration(duration);
                mAnimator.start();
            }else if(event.getActionMasked() == MotionEvent.ACTION_CANCEL ||
                    event.getActionMasked() == MotionEvent.ACTION_UP){
                if(isRunning) {
                    if(isComplete) {
                        fadeOff();
                    }else{
                        markForDelete = true;
                    }
                }
            }
        }

        public void setDuration(long millis){
            this.duration = millis;
        }

        public void setAlpha(int alpha){
            this.alpha = alpha;
            if(alpha > 255)
                mPaint.setAlpha(255);
            else if (alpha < 0)
                mPaint.setAlpha(0);
            else
                mPaint.setAlpha(alpha);
        }

        public void setRadius(float radius){
            mRadius = radius;
            v.invalidate();
        }

        public void setColor(int color){
            mPaint.setColor(color);
        }

        private void fadeOff(){
            mFadeAnimator = ValueAnimator.ofInt(mPaint.getAlpha(), 0);
            mFadeAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}

                @Override
                public void onAnimationEnd(Animator animator) {
                    isRunning = false;
                }

                @Override
                public void onAnimationCancel(Animator animator) {}

                @Override
                public void onAnimationRepeat(Animator animator) {}
            });
            mFadeAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    mPaint.setAlpha((Integer)valueAnimator.getAnimatedValue());
                    v.invalidate();
                }
            });
            mFadeAnimator.setInterpolator(new AccelerateInterpolator());
            mFadeAnimator.setDuration(duration);
            mFadeAnimator.start();
        }

        public void onDraw(Canvas canvas){
            if(isRunning) {
                canvas.drawCircle(mDownX, mDownY, mRadius, mPaint);
            }
        }

    }

    /*
    *   A simple dialog to mimic dialog in Material Design
    *
    *   - Use setListener if you need to know when user click on the button, this is optional
    *   - Any button click will cause the dialog to close
    *   - You must called setup before showing the dialog
    *   - You can skip mButtonCancelText(by setting it to null) to show just 1 option
    *
    *
    * */
    public static class MaterialDialog extends DialogFragment {

        private String mTitle;
        private String mMessage;
        private String mButtonOkText;
        private String mButtonCancelText;
        private Listener listener;

        public interface Listener {
            public void onButtonOkPressed();

            public void onButtonCancelPressed();
        }

        public void setListener(Listener listener) {
            this.listener = listener;
        }

        public void setup(String mTitle, String mMessage, String mButtonOkText, String mButtonCancelText){
            this.mTitle = mTitle;
            this.mMessage= mMessage;
            this.mButtonOkText = mButtonOkText;
            this.mButtonCancelText = mButtonCancelText;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

            View v = inflater.inflate(R.layout.dialog_material, container, false);

            TextView title = (TextView) v.findViewById(R.id.title);
            TextView message = (TextView) v.findViewById(R.id.message);
            RippleTextView btnOk = (RippleTextView) v.findViewById(R.id.btnOk);
            RippleTextView btnCancel = (RippleTextView) v.findViewById(R.id.btnCancel);

            btnOk.getRippleHelper().setColor(getResources().getColor(R.color.litegrey));
            btnCancel.getRippleHelper().setColor(getResources().getColor(R.color.litegrey));

            title.setText(mTitle);
            message.setText(mMessage);

            btnOk.setText(mButtonOkText);

            if(mButtonCancelText != null)
                btnCancel.setText(mButtonCancelText);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null)
                        listener.onButtonOkPressed();
                    getDialog().dismiss();
                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null)
                        listener.onButtonCancelPressed();
                    getDialog().dismiss();
                }
            });

            return v;
        }
    }

    public static class MaterialProgressDialog extends Dialog {

        private String mMessage;

        Handler handler = new Handler(new Handler.Callback()
        {
            @Override
            public boolean handleMessage(android.os.Message msg){
                close();
                return true;
            }
        });

        public MaterialProgressDialog(Context context, String message) {
            super(context);
            this.mMessage = message;
            init();
        }

        void init(){
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_material_progress);

            TextView message = (TextView) findViewById(R.id.message);
            message.setText(mMessage);
        }

        public void dismiss(){
            handler.sendEmptyMessage(0);
        }

        private void close(){
            super.dismiss();
        }
    }
}
