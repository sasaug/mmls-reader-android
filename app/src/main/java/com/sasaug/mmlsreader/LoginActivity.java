package com.sasaug.mmlsreader;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sasaug.mmlsreader.core.Engine;
import com.sasaug.mmlsreader.core.EngineInterface;
import com.sasaug.mmlsreader.core.objs.Announcement;
import com.sasaug.mmlsreader.core.objs.Subject;
import com.sasaug.mmlsreader.data.User;
import com.sasaug.mmlsreader.util.MaterialManager;

import java.util.ArrayList;


public class LoginActivity extends ActionBarActivity {

    Toolbar toolbar;

    EditText username;
    EditText password;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(User.getUsername() != null && !User.getUsername().equals("")){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        //setup the toolbar to replace actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.login));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);


        //link up the view to code
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        //add click listener to btnLogin so this code will trigger when we click btnLogin
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //trim removes space/new line in front and back of the string
                final String u = username.getText().toString().trim();
                final String p = password.getText().toString().trim();
                //first, check if username and password is empty

                if( u.isEmpty() || p.isEmpty()){
                    //display an toast, small black message to inform user about it, add return to block this code from executing further
                    Toast.makeText(LoginActivity.this, R.string.error_emptyuserpass, Toast.LENGTH_LONG).show();
                    return;
                }

                //now username and password is not empty, we can proceed to try to use this detail to login MMLS
                final MaterialManager.MaterialProgressDialog dialog = new MaterialManager.MaterialProgressDialog(LoginActivity.this, getString(R.string.attempting_login));
                dialog.setCancelable(false);
                dialog.show();

                //now we try to login using fetch data function in Engine class
                class Callback implements EngineInterface {
                    Handler failHandler = new Handler(new Handler.Callback()
                    {
                        @Override
                        public boolean handleMessage(android.os.Message msg){
                            Toast.makeText(LoginActivity.this, R.string.error_loginfailed, Toast.LENGTH_LONG).show();
                            return true;
                        }
                    });

                    Handler errorHandler = new Handler(new Handler.Callback()
                    {
                        @Override
                        public boolean handleMessage(android.os.Message msg){
                            Toast.makeText(LoginActivity.this, R.string.error_network, Toast.LENGTH_LONG).show();
                            return true;
                        }
                    });
                    @Override
                    public void onFetchData(boolean success, ArrayList<Announcement> announcements, ArrayList<Subject> subjects) {
                        dialog.dismiss();
                        if(success){
                            //login success, lets store this username and password
                            User.setUsername(u);
                            User.setPassword(p);

                            //open the next activity and kill this activity off
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }else{
                            failHandler.sendEmptyMessage(0);
                        }
                    }

                    @Override
                    public void onError() {
                        dialog.dismiss();
                        errorHandler.sendEmptyMessage(0);
                    }
                }
                Engine.fetchData(LoginActivity.this, u, p, new Callback());
            }
        });

    }
}
