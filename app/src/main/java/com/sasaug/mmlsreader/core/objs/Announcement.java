package com.sasaug.mmlsreader.core.objs;

/**
 * Created by sasaug on 10/26/14.
 */
public class Announcement {
    public String title;
    public String date;
    public String subject;
    public String author;
    public String msg;
}
