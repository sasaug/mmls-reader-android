package com.sasaug.network.common;

import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.impl.client.DefaultHttpClient;

public interface WebRequestIFace {
	void onComplete(HttpResponse response, DefaultHttpClient client);
	void onGetResult(String result);
	void onGetResult(byte[] result);
	void onGetResult(InputStream result);
	void onGetResult(String result, StatusLine status);
	void onGetResult(byte[] result, StatusLine status);
	void onGetResult(InputStream result, StatusLine status);
	void onError(String error);
	void onError(Exception ex);
	void onAllResultDone(ArrayList<String> results);
	void onRedirected(String location);
	void onDownloadProgressChange(long downloaded, long total);
	
	ArrayList<NameValue> setHeaders();
	DefaultHttpClient setDefaultHttpClient();
	String setAuthUsername();
	String setAuthPassword();
	boolean setHandleRedirect();
}
