package com.sasaug.network.common;

public class NetTask {
	public int pid;
	public String tag;
	public WebCall task;
	
	NetTask(int pid, String tag, WebCall task)
	{
		this.pid = pid;
		this.tag = tag;
		this.task = task;		
	}
	
	
	public void killTask()
	{
		//Attempt to kill WebCall
		try
		{
			if(task.isAlive())
				task.shouldRun = false;
		}
		catch(Exception ex)
		{
			
		}
	}
}
