package com.sasaug.network.common;

import java.util.ArrayList;

public class NetGroup {
	public int taskDone = 0;
	public boolean casted = false;
	public ArrayList<WebRequestIFace> request = new ArrayList<WebRequestIFace>();
	
	//Result related
	public ArrayList<String> result = new ArrayList<String>();
	public ArrayList<byte[]> result_data = new ArrayList<byte[]>();
	public ArrayList<Integer> result_id = new ArrayList<Integer>();
}
