package com.sasaug.network.common;

public class NameValue {

	public String name;
	public String value;
	
	public NameValue(String name, String value)
	{
		this.name = name;
		this.value = value;
	}

}
