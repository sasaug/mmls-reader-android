package com.sasaug.network.common;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.*;

public class WebCall extends Thread{
	//constant
	static int STATE_NULL = -1;
	static int STATE_READY = 0;
	static int STATE_RUNNING = 1;
	static int STATE_ENDED = 2;
	static String CHARSET_ENCODING = "UTF-8";
	
	static char METHOD_GET = 'a';
	static char METHOD_POST = 'b';
	static char METHOD_GET_BINARY = 'c';
	static char METHOD_POST_BINARY = 'd';
	static char METHOD_GET_STREAM = 'e';
	static char METHOD_POST_STREAM = 'f';
	static char METHOD_PUT = 'g';
	static char METHOD_POST_STRING = 'h';
	
	static String USER_AGENT = "Mozilla/5.0";
	
	//state
	public int state = STATE_NULL;
	
	//method
	public char method = METHOD_GET;
	
	//data
	public int pid;
	String url = "";
	List<NameValuePair> parameters = null;
	String json = "";
	List<String> header_name = new ArrayList<String>();
	List<String> header_value = new ArrayList<String>();
	public boolean shouldRun = true;
	
	boolean isSSLEnabled = false;
	String auth_username = null;
	String auth_password = null;
	DefaultHttpClient client = null;
	boolean handleRedirect = true;

	//Callback
	ArrayList<WebCallIFace> callbacks = new ArrayList<WebCallIFace>();
	
	public static int TIMEOUT = 10000;
	public static int READ_TIMEOUT = 60000;
	
	
	
	public WebCall(WebCallIFace request, String url, char method)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.state = STATE_READY;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, String json, char method)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		this.json = json;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.state = STATE_READY;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, String json, char method, String username, String password)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		this.json = json;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.state = STATE_READY;
		this.auth_username = username;
		this.auth_password = password;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, char method, String username, String password)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.state = STATE_READY;
		this.auth_username = username;
		this.auth_password = password;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, char method, List<NameValuePair> api_parameters)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.parameters = api_parameters;
		this.state = STATE_READY;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, char method, String json)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.json = json;
		this.state = STATE_READY;

		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, char method, String json, String username, String password)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.json = json;
		this.state = STATE_READY;
		this.auth_username = username;
		this.auth_password = password;

		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public WebCall(WebCallIFace request, String url, char method, List<NameValuePair> parameters, String username, String password)
	{
		this.callbacks.add(request);
		this.url = url;
		this.method = method;
		Random randomGenerator = new Random();
		this.pid = randomGenerator.nextInt(10000000);
		this.parameters = parameters;
		this.state = STATE_READY;
		this.auth_username = username;
		this.auth_password = password;
		
		if(url.startsWith("https://"))
			isSSLEnabled = true;
		
		processRequest(request);
	}
	
	public void addHeader(String name, String value)
	{
		header_name.add(name);
		header_value.add(value);
	}
	
	void processRequest(WebCallIFace request)
	{
		//add headers request
		ArrayList<NameValue> headers = request.setHeaders();
		for(int i = 0; i < headers.size(); i++)
		{
			addHeader(headers.get(i).name, headers.get(i).value);
		}	
		
		//override http client
		client = request.setDefaultHttpClient();
		
		handleRedirect = request.setRedirectHandler();
		
	}
		
	public void run()
	{
		state = STATE_RUNNING;
		if(method == METHOD_GET)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = getHTML(url, useAuth);
			    String result = new String(consumeContent(response), CHARSET_ENCODING);
				
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, result, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if (method == METHOD_POST)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = execPost(url, parameters, useAuth);
				String result = new String(consumeContent(response), CHARSET_ENCODING);
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, result, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if(method == METHOD_GET_BINARY)
		{	
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = getHTML(url, useAuth);
				byte[] data = consumeContent(response);
			    
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, data, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if(method == METHOD_POST_BINARY)
		{	
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = execPost(url, parameters, useAuth);
				byte[] data = consumeContent(response);
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, data, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if(method == METHOD_GET_STREAM)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = getHTML(url, useAuth);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, is, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if(method == METHOD_POST_STREAM)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = execPost(url, parameters, useAuth);
				HttpEntity entity = response.getEntity();
				InputStream stream = entity.getContent();
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, stream, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if(method == METHOD_POST_STRING)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = execPostString(url, json, useAuth);
				String result = new String(consumeContent(response), CHARSET_ENCODING);
				
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, result, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		else if (method == METHOD_PUT)
		{
			try
			{
				boolean useAuth = false;
				if(auth_username != null)
					useAuth = true;
				HttpResponse response = execPut(url, json, useAuth);
				String result = new String(consumeContent(response), CHARSET_ENCODING);
				
				if(shouldRun)
				{
					for(int i =0; i < callbacks.size();i++)
					{
						callbacks.get(i).onComplete(pid, result, client, response);
					}
				}
			}catch(Exception ex)
			{
				for(int i =0; i < callbacks.size();i++)
				{
					callbacks.get(i).onError(pid, ex);
				}
			}
		}
		state = STATE_ENDED;
	}
	
	HttpResponse getHTML(String url, boolean useAuth) throws Exception
    {
	      try
	        {
	    	    HttpGet get = new HttpGet(url);
	    	    get.setHeader("User-Agent", USER_AGENT);
	    	    for(int i = 0; i < header_name.size(); i++)
	    	    	get.setHeader(header_name.get(i), header_value.get(i));
	            HttpParams httpParams = new BasicHttpParams();
	            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
	            HttpConnectionParams.setSoTimeout(httpParams, READ_TIMEOUT);
	            httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, handleRedirect);
	            
	            if(client == null)
	            	client = new DefaultHttpClient(httpParams);

	            if(handleRedirect)
	            {
		            class CustomRedirectHandler extends DefaultRedirectHandler {
		                public URI getLocationURI(HttpResponse response, HttpContext context) {
		                	Header[] headers = response.getHeaders("Location");
		                	String location = headers[0].getValue();
		                	for(int i =0; i < callbacks.size();i++)
							{
								callbacks.get(i).onRedirected(location);
							}
		                	URI url = null;
							try {
								url = new URI(location);
							} catch (URISyntaxException e) {
								e.printStackTrace();
							}
		                    return url;
		                }
		            }
		            client.setRedirectHandler(new CustomRedirectHandler());
	            }
	            
	            if(useAuth)
	            {
		            client.getCredentialsProvider().setCredentials(
		                    new AuthScope(getHostAddress(url), 80),
		                    new UsernamePasswordCredentials(auth_username, auth_password));
	            }
	            if(isSSLEnabled)
	        	{
		            SchemeRegistry schemaRegistry = client.getConnectionManager().getSchemeRegistry();
		            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
		            trustStore.load(null, null);
		            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
		            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		            schemaRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		            schemaRegistry.register(new Scheme("https", sf, 80));
		            schemaRegistry.register(new Scheme("https", sf, 8080));
		            schemaRegistry.register(new Scheme("https", sf, 8090));
		            schemaRegistry.register(new Scheme("https", sf, 443));
	        	}
	            HttpResponse response = client.execute(get);
	            return response;
	            
	        }catch(Exception ex)
	        {
	        	throw ex;
	        }
    }

	HttpResponse execPost(String url, List<NameValuePair> nameValuePairs, boolean useAuth) throws Exception
    {
        try
        {
    	    HttpPost post = new HttpPost(url);
    	    post.setHeader("User-Agent", USER_AGENT);
    	    for(int i = 0; i < header_name.size(); i++)
    	    	post.setHeader(header_name.get(i), header_value.get(i));
    	    if(nameValuePairs.size() != 0)
    	    	post.setEntity(new UrlEncodedFormEntity(nameValuePairs, CHARSET_ENCODING));
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, READ_TIMEOUT);
            httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, handleRedirect);
            
            if(client == null)
            	client = new DefaultHttpClient(httpParams);
            
            if(handleRedirect)
            {
	            class CustomRedirectHandler extends DefaultRedirectHandler {
	                public URI getLocationURI(HttpResponse response, HttpContext context) {
	                	Header[] headers = response.getHeaders("Location");
	                	String location = headers[0].getValue();
	                	for(int i =0; i < callbacks.size();i++)
						{
							callbacks.get(i).onRedirected(location);
						}
	                	URI url = null;
						try {
							url = new URI(location);
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
	                    return url;
	                }
	            }
	            client.setRedirectHandler(new CustomRedirectHandler());
            }
            
            if(useAuth)
            {
	            client.getCredentialsProvider().setCredentials(
	                    new AuthScope(getHostAddress(url), 80),
	                    new UsernamePasswordCredentials(auth_username, auth_password));
            }
            if(isSSLEnabled)
        	{
            	SchemeRegistry schemaRegistry = client.getConnectionManager().getSchemeRegistry();
	            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	            trustStore.load(null, null);
	            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            schemaRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	            schemaRegistry.register(new Scheme("https", sf, 80));
	            schemaRegistry.register(new Scheme("https", sf, 8080));
	            schemaRegistry.register(new Scheme("https", sf, 8090));
	            schemaRegistry.register(new Scheme("https", sf, 443));
        	}
            
            HttpResponse response = client.execute(post);
            return response;
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        	throw ex;
        }
    }
	
	HttpResponse execPostString(String url, String data, boolean useAuth) throws Exception
    {
        try
        {
    	    HttpPost post = new HttpPost(url);
    	    post.setHeader("User-Agent", USER_AGENT);
    	    for(int i = 0; i < header_name.size(); i++)
    	    	post.setHeader(header_name.get(i), header_value.get(i));
    	    StringEntity entity = new StringEntity(data, CHARSET_ENCODING);
    	    post.setEntity(entity);
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, READ_TIMEOUT);
            httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, handleRedirect);
            
            if(client == null)
            	client = new DefaultHttpClient(httpParams);
            
            if(handleRedirect)
            {
	            class CustomRedirectHandler extends DefaultRedirectHandler {
	                public URI getLocationURI(HttpResponse response, HttpContext context) {
	                	Header[] headers = response.getHeaders("Location");
	                	String location = headers[0].getValue();
	                	for(int i =0; i < callbacks.size();i++)
						{
							callbacks.get(i).onRedirected(location);
						}
	                	URI url = null;
						try {
							url = new URI(location);
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
	                    return url;
	                }
	            }
	            client.setRedirectHandler(new CustomRedirectHandler());
            }
            
            if(useAuth)
            {
	            client.getCredentialsProvider().setCredentials(
	                    new AuthScope(getHostAddress(url), 80),
	                    new UsernamePasswordCredentials(auth_username, auth_password));
            }
            if(isSSLEnabled)
        	{
            	SchemeRegistry schemaRegistry = client.getConnectionManager().getSchemeRegistry();
	            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	            trustStore.load(null, null);
	            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            schemaRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	            schemaRegistry.register(new Scheme("https", sf, 80));
	            schemaRegistry.register(new Scheme("https", sf, 8080));
	            schemaRegistry.register(new Scheme("https", sf, 8090));
	            schemaRegistry.register(new Scheme("https", sf, 443));
        	}
            HttpResponse response = client.execute(post);
            return response;
        }catch(Exception ex)
        {
        	throw ex;
        }
    }
	
	HttpResponse execPut(String url, String json, boolean useAuth) throws Exception
    {
        try
        {
    	    HttpPut put = new HttpPut(url);
    	    put.setHeader("User-Agent", USER_AGENT);
    	    for(int i = 0; i < header_name.size(); i++)
    	    	put.setHeader(header_name.get(i), header_value.get(i));
    	    put.setEntity(new StringEntity(json, CHARSET_ENCODING));
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, READ_TIMEOUT);
            httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, handleRedirect);
            
            if(client == null)
            	client = new DefaultHttpClient(httpParams);
            
            if(handleRedirect)
            {
	            class CustomRedirectHandler extends DefaultRedirectHandler {
	                public URI getLocationURI(HttpResponse response, HttpContext context) {
	                	Header[] headers = response.getHeaders("Location");
	                	String location = headers[0].getValue();
	                	for(int i =0; i < callbacks.size();i++)
						{
							callbacks.get(i).onRedirected(location);
						}
	                	URI url = null;
						try {
							url = new URI(location);
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
	                    return url;
	                }
	            }
	            client.setRedirectHandler(new CustomRedirectHandler());
            }
            
            if(useAuth)
            {
	            client.getCredentialsProvider().setCredentials(
	                    new AuthScope(getHostAddress(url), 80),
	                    new UsernamePasswordCredentials(auth_username, auth_password));
            }
            if(isSSLEnabled)
        	{
            	SchemeRegistry schemaRegistry = client.getConnectionManager().getSchemeRegistry();
	            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	            trustStore.load(null, null);
	            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            schemaRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	            schemaRegistry.register(new Scheme("https", sf, 80));
	            schemaRegistry.register(new Scheme("https", sf, 8080));
	            schemaRegistry.register(new Scheme("https", sf, 8090));
	            schemaRegistry.register(new Scheme("https", sf, 443));
        	}
            
            HttpResponse response = client.execute(put);
            return response;
        }catch(Exception ex)
        {
        	throw ex;
        }
    }

	private String getHostAddress(String url) throws Exception
	{
		URI newUrl = new URI(url);
		return newUrl.getHost();
	}
	
	private byte[] consumeContent(HttpResponse response) throws Exception
	{
		Header[] clHeaders = response.getHeaders("Content-Length");
		int totalSize = 0;
		if(clHeaders.length != 0)
		{	
			Header header = clHeaders[0];
			totalSize = Integer.parseInt(header.getValue());
		}
	    InputStream stream = response.getEntity().getContent();
	    byte buf[] = new byte[1024 * 1024];
	    int numBytesRead;
	    int downloadedSize = 0;
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    BufferedOutputStream fos = new BufferedOutputStream(bos);
	    do {
	        numBytesRead = stream.read(buf);
	        if (numBytesRead > 0) {
	            fos.write(buf, 0, numBytesRead);
	            downloadedSize += numBytesRead;
	            for(int i =0; i < callbacks.size();i++)
				{
	            	if(callbacks.get(i).useThreadedDownloadProgressCallback())
	            	{
	            		class ThreadedCallback extends Thread
	            		{
	            			int i;
	            			long downloadedSize;
	            			long totalSize;
	            			ThreadedCallback(int i, long downloadedSize, long totalSize)
	            			{
	            				this.i = i;
	            				this.downloadedSize = downloadedSize;
	            				this.totalSize = totalSize;
	            			}
	            			
	            			public void run()
	            			{
	            				callbacks.get(i).onDownloadProgressChange(downloadedSize, totalSize);
	            			}
	            		}
	            		new ThreadedCallback(i, downloadedSize, totalSize).start();
	            	}
	            	else
	            	{
	            		callbacks.get(i).onDownloadProgressChange(downloadedSize, totalSize);
	            	}
				}
	        }
	        
	    } while (numBytesRead > 0);
	    fos.flush();
	    byte[] data = bos.toByteArray();
	    fos.close();
	    stream.close();
	    
	    return data;
	}
}

